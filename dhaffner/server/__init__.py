from collections import namedtuple

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS
from flask_jwt import JWT

from authomatic import Authomatic

from .helpers import make_celery, create_test_user


Components = namedtuple(
    'Components',
    ('app', 'db', 'cors', 'authomatic', 'jwt', 'celery')
)
db = SQLAlchemy()


class Server(object):
    modules = {}

    def __init__(self, name=None, config=None, models=None, modules=None):
        if modules is None:
            modules = {}
        elif isinstance(modules, list):
            modules = dict.fromkeys(modules)
        elif isinstance(modules, dict):
            modules = {(k, v) for (k, v) in dict.items() if v}

        self.app = app = Flask(name)
        app.config.from_object(config)
        db.init_app(app)
        self.db = db

        self.models = models
        from . import models as base_models

        self.init_modules(modules)

        @app.before_first_request
        def before_first_request():
            app.logger.info('First request: %s', request.url)
            try:
                db.create_all()
            except:
                app.logger.exception("Couldn't create models...")
            else:
                app.logger.info('Created all models successfully.')
                if app.config['DEBUG'] is True:
                    app.logger.debug('Creating a test user account.')
                    self.create_test_user(db, models.User)

    def init_modules(self, modules, force=True):
        for (name, value) in modules:
            if self.modules.get(name) is not None and force is False:
                continue

            func = None
            if callable(value):
                func = value
            elif value is True:
                try:
                    func = getattr(self, f'get_{name}')
                except AttributeError:
                    self.app.logger.exception('Module was specified but not found: %s', name)
            elif value is False:
                self.app.logger.debug(f'Not initializing module {name}')

            if func:
                self.modules[name] = func()
                self.app.logger.info(f'Initialozed module {name}')

    def get_celery(self):
        return make_celery(self.app)

    def get_cors(self):
        return CORS(
            self.app,
            origins=self.app.config.CORS_ORIGINS,
            expose_headers='Location',
            supports_credentials=True
        )

    def get_authomatic(self):
        return Authomatic(
            self.app.config.AUTHOMATIC_CONFIG, self.app.config.SECRET_KEY)

    def get_jwt(self):
        return JWT(self.app, self.models.User.User.authenticate, self.models.User.identify)

    def get_marshmallow(self):
        return Marshmallow(self.app)
