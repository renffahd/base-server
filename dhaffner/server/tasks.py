import time

from . import celery


@celery.task(name='default')
def default():
    return time.time()
