from sqlalchemy.dialects.postgresql import JSON
from werkzeug.security import check_password_hash, generate_password_hash

from . import db


user_role = db.Table(
    'user_role',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class Role(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model):
    def __init__(self, *args, **kwargs):
        password = kwargs.pop('password', None)
        super(User, self).__init__(*args, **kwargs)
        if password is not None:
            self.set_password(password)

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())

    confirmed_at = db.Column(db.DateTime())

    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())

    last_login_ip = db.Column(db.String(100))
    current_login_ip = db.Column(db.String(100))

    login_count = db.Column(db.Integer)
    preferences = db.Column(JSON, default=dict)

    roles = db.relationship(
        'Role',
        secondary=user_role,
        backref=db.backref('users', lazy='dynamic')
    )

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __str__(self):
        return '<User {}>'.format(self.email)

    @staticmethod
    def authenticate(username, password):
        user = User.query.filter(User.username == username).first()
        if user.check_password(password):
            return user

    @staticmethod
    def identify(payload):
        return User.query.filter(User.id == payload['identity']).first()
