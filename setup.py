"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""
from setuptools import setup


setup(
    packages=['dhaffner.server'],
    name='dhaffner.server',
    version='0.0.1',
    description='A base Python package.',
    url='https://github.com/dhaffner/base',
    author='Dustin Haffner',
    author_email='dh@xix.org',
    license='MIT',
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    keywords='sample setuptools development',
    install_requires=[
        "flask",
        "flask-sqlalchemy",
        "flask-marshmallow",
        "flask-cors",
        "flask-jwt",
        "authomatic",
        "twisted",
        "autobahn[twisted]",
        "psycopg2",
        "celery",
        "requests"
    ]
)
